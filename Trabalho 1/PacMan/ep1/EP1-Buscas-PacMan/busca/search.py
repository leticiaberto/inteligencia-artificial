# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).



"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import copy

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    # ja alcancou o objetivo?
    alcancouObjetivo = False
    # ja explorou tudo?
    explorouTodos = False

    # estado de inicio
    estado_inicial = problem.getStartState()
    #registro dos estados visitados
    # util.py linha 236 class Counter (extensao do dicionario python, facilita contagens)
    estados_visitados = util.Counter()
    # estado inicial eh atribuido a essa vetor de estados visitados
    estados_visitados[0] = estado_inicial

    # estados vizinhos (fronteira)
    estados_fronteira = util.Counter()
    # sucessores
    estados_sucessores = problem.getSuccessors(problem.getStartState())

    # lista de vertices, como chaves
    lista_vertices = {}

    # pilha para mater os estados da fronteira (o primeiro que entra eh o primeiro que sai - que sera visitado)
    stack_fronteira = util.Stack()
    # fila que contem a lista de acoes
    fila_acoes = []

    # colocar todos os estados fronteira na pilha
    for i in estados_sucessores:
        stack_fronteira.push(i)

    for i in estados_sucessores:
        acoes_feitas = copy.deepcopy(fila_acoes)
        successor = str(i[0])
        lista_vertices[successor] = acoes_feitas

    foi_visitado = 1

    while alcancouObjetivo == False:
        # pega o proximo no para verificar, sendo ele o primeiro da pilha
        no_temporario = stack_fronteira.pop()
        proximo_no = no_temporario[0]
        # salva a acao feita para chegar nesse no
        proxima_acao = no_temporario[1]

        #salva o no explorado
        estados_visitados[foi_visitado] = proximo_no
        #salva a acao realizada
        fila_acoes.append(proxima_acao)

        foi_visitado = foi_visitado + 1

        #proximo_no agora eh o no_atual
        if (explorouTodos == True):
            inc = 0
            otherInc = 0

        reset = str(no_temporario[0])

        aux_fila_acoes = lista_vertices[reset]
        aux_fila_acoes.append(no_temporario[1])

        # limpa a fila de acoes antiga
        fila_acoes = copy.deepcopy(aux_fila_acoes)

        no_atual = proximo_no

        # verifica se o no eh o objetivo
        if(problem.isGoalState(no_atual)):
            alcancouObjetivo = True
        else: #pega o proximo no da stack_fronteira
            estados_sucessores = problem.getSuccessors(no_atual)
            for i in estados_sucessores:
                acoes_feitas = copy.deepcopy(fila_acoes)
                successor = str(i[0])
                lista_vertices[successor] = acoes_feitas

            primeiro_no = estados_sucessores[0]

            no_visitado = False
            # para contar as interacoes feitas
            contador = 0

            for i in estados_sucessores:
                no_visitado = False
                explorouTodos = False
                for j in estados_visitados:
                    estado_corrente = estados_visitados[j]

                    if(i[0] == estado_corrente):
                        no_visitado = True
                        contador = contador + 1

                        if(contador == (len(estados_sucessores))):
                            explorouTodos = True

                    elif((no_visitado == False) and (j == (len(estados_visitados) - 1))):
                        stack_fronteira.push(i)

    return fila_acoes

    #util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    alcancouObjetivo = False
    explorouTodos = False
    estado_inicial = problem.getStartState()
    estados_visitados = {}
    # estado inicial eh armazenado no vetor de estados visitados
    estados_visitados[0] = estado_inicial
    # getSuccessors retorna uma tripa (successor, action, stepCost)
    estados_sucessores = problem.getSuccessors(estado_inicial)
    lista_vertices = {}
    stack_fronteira = util.Queue()
    fila_acoes = []
    # lista para armazenar os nos que foram explorados
    nos_adicionados = []

    #estados sucessores na pilha
    for i in estados_sucessores:
        stack_fronteira.push(i)
        nos_adicionados.append(i[0])

    for i in estados_sucessores:
        # The difference between shallow and deep copying is only relevant for
        # compound objects (objects that contain other objects, like lists or class instances)
        acoes_feitas = copy.deepcopy(fila_acoes)
        successor = str(i[0])

        lista_vertices[successor] = acoes_feitas

    foi_visitado = 1
    contador = 0

    while alcancouObjetivo == False:
        contador = contador + 1

        for i in nos_adicionados:
            aux_pop = nos_adicionados.pop()
            estados_visitados[foi_visitado] = aux_pop
            foi_visitado = foi_visitado + 1

        no_temporario = stack_fronteira.pop()
        proximo_no = no_temporario[0]
        proxima_acao = no_temporario[1]

        reset = str(no_temporario[0])
        aux_fila_acoes = lista_vertices[reset]
        aux_fila_acoes.append(proxima_acao)
        fila_acoes = copy.deepcopy(aux_fila_acoes)

        no_atual = proximo_no

        if(problem.isGoalState(no_atual)):
            alcancouObjetivo = True
        else:
            estados_sucessores = problem.getSuccessors(no_atual)

            for i in estados_sucessores:
                no_visitado = False
                count = 0

                for j in estados_visitados:
                    estado_corrente = estados_visitados[j]

                    if(i[0] == estado_corrente):
                        no_visitado = 1
                        count = count + 1
                    elif((no_visitado == False) and (j == len(estados_visitados) - 1)):
                        acoes_feitas = copy.deepcopy(fila_acoes)
                        sucessor = str(i[0])

                        lista_vertices[sucessor] = acoes_feitas

                        stack_fronteira.push(i)
                        nos_adicionados.append(i[0])

    return fila_acoes


    #util.raiseNotDefined()

def iterativeDeepeningSearch(problem):
    """
    Start with depth = 0.
    Search the deepest nodes in the search tree first up to a given depth.
    If solution not found, increment depth limit and start over.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.
    """
    "*** YOUR CODE HERE ***"
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
"""
    # ja alcancou o objetivo?
    alcancouObjetivo = False
    # ja explorou tudo?
    explorouTodos = False

    # estado de inicio
    estado_inicial = problem.getStartState()
    #registro dos estados visitados
    # util.py linha 236 class Counter (extensao do dicionario python, facilita contagens)
    estados_visitados = util.Counter()
    # estado inicial eh atribuido a essa vetor de estados visitados
    estados_visitados[0] = estado_inicial

    # estados vizinhos (fronteira)
    estados_fronteira = util.Counter()
    # sucessores
    # getSuccessors retorna uma tripa (successor, action, stepCost)
    estados_sucessores = problem.getSuccessors(problem.getStartState())

    # lista de vertices, como chaves
    lista_vertices = {}

    # pilha para mater os estados da fronteira (o primeiro que entra eh o primeiro que sai - que sera visitado)
    stack_fronteira = util.Stack()
    # fila que contem a lista de acoes
    fila_acoes = []

    # sera incrementado cada vez que nao encontrar nenhum resultado
    nivel = 0;

    # colocar todos os estados fronteira na pilha
    for i in estados_sucessores:
        stack_fronteira.push(i)

    for i in estados_sucessores:
        acoes_feitas = copy.deepcopy(fila_acoes)
        successor = str(i[0])
        lista_vertices[successor] = acoes_feitas

    foi_visitado = 1

    while alcancouObjetivo == False:
        aux_nivel = 0
        # pega o proximo no para verificar, sendo ele o primeiro da pilha
        no_temporario = stack_fronteira.pop()
        proximo_no = no_temporario[0]
        # salva a acao feita para chegar nesse no
        proxima_acao = no_temporario[1]

        #salva o no explorado
        estados_visitados[foi_visitado] = proximo_no
        #salva a acao realizada
        fila_acoes.append(proxima_acao)

        foi_visitado = foi_visitado + 1

        #proximo_no agora eh o no_atual
        if (explorouTodos == True):
            inc = 0
            otherInc = 0

        reset = str(no_temporario[0])

        aux_fila_acoes = lista_vertices[reset]
        aux_fila_acoes.append(no_temporario[1])

        # limpa a fila de acoes antiga
        fila_acoes = copy.deepcopy(aux_fila_acoes)

        no_atual = proximo_no

        # verifica se o no eh o objetivo
        if(problem.isGoalState(no_atual)):
            alcancouObjetivo = True
        else: #pega o proximo no da stack_fronteira
            estados_sucessores = problem.getSuccessors(no_atual)
            for i in estados_sucessores:
                acoes_feitas = copy.deepcopy(fila_acoes)
                successor = str(i[0])
                lista_vertices[successor] = acoes_feitas

            primeiro_no = estados_sucessores[0]

            no_visitado = False
            # para contar as interacoes feitas
            contador = 0

            for i in estados_sucessores:
                aux_nivel = aux_nivel + 1
                if(aux_nivel <= nivel):
                    no_visitado = False
                    explorouTodos = False
                    for j in estados_visitados:
                        estado_corrente = estados_visitados[j]

                        if(i[0] == estado_corrente):
                            no_visitado = True
                            contador = contador + 1

                            if(contador == (len(estados_sucessores))):
                                explorouTodos = True

                        elif((no_visitado == False) and (j == (len(estados_visitados) - 1))):
                            stack_fronteira.push(i)

        nivel = nivel + 1

    return fila_acoes
    #util.raiseNotDefined()
"""
def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    # ja alcancou o objetivo?
    alcancouObjetivo = False
    # ja explorou tudo?
    explorouTodos = False
    # custo do caminho anterior
    custo_anterior = 0

    # estado de inicio
    estado_inicial = problem.getStartState()
    #registro dos estados visitados
    # util.py linha 236 class Counter (extensao do dicionario python, facilita contagens)
    estados_visitados = util.Counter()
    # estado inicial eh atribuido a essa vetor de estados visitados
    estados_visitados[0] = estado_inicial

    # estados vizinhos (fronteira)
    estados_fronteira = util.Counter()
    # sucessores
    estados_sucessores = problem.getSuccessors(problem.getStartState())

    # lista de vertices, como chaves
    lista_vertices = {}

    # pilha para mater os estados da fronteira (o primeiro que entra eh o primeiro que sai - que sera visitado)
    fila_prioritaria = util.PriorityQueue()
    # fila que contem a lista de acoes
    fila_acoes = []
    nos_adicionados = []

    for i in estados_sucessores:
        acoes_feitas = copy.deepcopy(fila_acoes)
        sucessor = str(i[0])
        lista_vertices[sucessor] = acoes_feitas

    for i in estados_sucessores:
        fila_prioritaria.push(i, i[2])
        nos_adicionados.append(i[0])

    foi_visitado = 1

    while alcancouObjetivo == False:
        for i in nos_adicionados:
            aux_pop = nos_adicionados.pop()
            estados_visitados[foi_visitado] = aux_pop
            foi_visitado = foi_visitado + 1

        no_temporario = fila_prioritaria.pop()
        custo_anterior = no_temporario[2]
        proximo_no = no_temporario[0]
        proxima_acao = no_temporario[1]

        estados_visitados[foi_visitado] = proximo_no
        foi_visitado = foi_visitado + 1

        reset = str(no_temporario[0])
        aux_fila_acoes = lista_vertices[reset]
        aux_fila_acoes.append(no_temporario[1])
        fila_acoes = copy.deepcopy(aux_fila_acoes)

        no_atual = proximo_no

        if(problem.isGoalState(no_atual)):
            alcancouObjetivo = True
        else:
            estados_sucessores = problem.getSuccessors(no_atual)
            for i in estados_sucessores:
                no_visitado = False
                contador = 0
                for j in estados_visitados:
                    estado_corrente = estados_visitados[j]
                    if(i[0] == estado_corrente):
                        no_visitado = True
                        contador = contador + 1
                    elif(no_visitado == False and (j == (len(estados_visitados) - 1))):
                        acoes_feitas = copy.deepcopy(fila_acoes)
                        sucessor = str(i[0])
                        lista_vertices[sucessor] = acoes_feitas

                        novo_custo = i[2] + custo_anterior
                        fNo = list(i)
                        fNo[2] = novo_custo
                        fNo = tuple(fNo)

                        fila_prioritaria.push(fNo, novo_custo)
                        nos_adicionados.append(fNo[0])

    tamanho = len(fila_acoes)
    return fila_acoes

    #util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    closedset = []
    fringe = util.PriorityQueue()
    start = problem.getStartState()
    fringe.push( (start, []), heuristic(start, problem))

    while not fringe.isEmpty():
        node, actions = fringe.pop()

        if problem.isGoalState(node):
            return actions

        closedset.append(node)

        for coord, direction, cost in problem.getSuccessors(node):
            if not coord in closedset:
                new_actions = actions + [direction]
                score = problem.getCostOfActions(new_actions) + heuristic(coord, problem)
                fringe.push( (coord, new_actions), score)

    return []
    #util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
ids = iterativeDeepeningSearch
